/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.client;

import com.zoomdata.AbstractZoomdataServerTest;
import com.zoomdata.driver.model.ZoomdataVersion;
import com.zoomdata.model.stream.StreamSource;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

public class ZoomdataAPIClientTest extends AbstractZoomdataServerTest {

    @Test
    public void testIndex() throws IOException {
        getServer().expect().withPath(BASE_API_PATH + "/")
                .andReturn(200, readTestFile("api/indexTest.json")).always();
        Map<String, Object> index = getAPIClient().index();
        assertFalse(index.isEmpty());
    }

    @Test
    public void testVersion() throws IOException {
        getServer().expect().withPath(BASE_API_PATH + "/version")
                .andReturn(200, readTestFile("api/versionTest.json")).always();
        ZoomdataVersion version = getAPIClient().zoomdataVersion();
        assertEquals(version.getVersion(), "2.6.0");
    }

    @Test
    public void testListSources() throws IOException {
        getServer().expect().withPath(BASE_API_PATH + "/sources")
                .andReturn(200, readTestFile("api/sourcesTest.json")).always();
        List<StreamSource> sources = getAPIClient().listSources();
        // One source of two is disabled. The API client shouldn't filter disabled sources
        assertEquals(sources.size(), 2);
        // Make sure sources have fields
        sources.stream().forEach(s -> {
            assertFalse(s.getObjectFields().isEmpty());
            s.getObjectFields().stream().forEach(f -> assertNotNull(f.getName()));
        });
    }

    @Test
    public void testGetSourceByName() throws IOException {
        String expectedSourceName = "SoCal Big Data Day";
        getServer().expect().withPath(BASE_API_PATH + "/sources?name=" + expectedSourceName.replaceAll("\\s", "%20"))
                .andReturn(200, readTestFile("api/sourceByNameTest.json")).always();
        StreamSource source = getAPIClient().getSourceByName(expectedSourceName);
        assertNotNull(source);
        assertEquals(source.getName(), expectedSourceName);
    }
}
