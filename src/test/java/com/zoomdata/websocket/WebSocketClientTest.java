/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.websocket;

import com.zoomdata.AbstractZoomdataServerTest;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.ExecutionException;

import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_PATH;
import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_PORT;

public class WebSocketClientTest extends AbstractZoomdataServerTest {

    @BeforeMethod
    public void setupWebSocketClient() {
        getServer().expect().withPath(BASE_WEB_SOCKET_PATH).andReturn(101, null).always();
        getServer().expect().get().withPath(BASE_WEB_SOCKET_PATH).andUpgradeToWebSocket().open();
    }

    @Test
    public void testWebSocketValidConnection() throws Exception {
        getWebSocketClient().connect();
        // No errors, success
    }

    @Test(expectedExceptions = ExecutionException.class)
    public void testWebSocketInvalidConnection() throws Exception {
        IWebSocketClient badClient = new ZoomdataWebSocketClient(null, null, false,
                "badhost", DEFAULT_PORT, DEFAULT_PATH);
        badClient.connect();
    }

    @Test
    public void testCloseOpenWebSocket() throws Exception {
        getWebSocketClient().connect();
        getWebSocketClient().close();
        // TODO should the client maintain state (isConnected())?
        // No errors, success
    }

    @Test
    public void testCloseWebSocketNeverStarted() throws Exception {
        getWebSocketClient().close();
        // No errors, success (a warning should've been logged)
    }

    @Test
    public void testCloseDisconnectedWebSocket() throws Exception {
        getWebSocketClient().connect();
        getWebSocketClient().close();
        getWebSocketClient().close();
        // No errors, success
    }

    @Test
    public void testFetchData() throws Exception {
        getWebSocketClient().connect();
        // TODO fetch data
        getWebSocketClient().fetch();
        Assert.fail();
        getWebSocketClient().close();
    }
}
