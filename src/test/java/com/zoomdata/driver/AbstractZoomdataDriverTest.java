/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver;

import com.zoomdata.AbstractZoomdataServerTest;
import org.testng.annotations.BeforeMethod;

import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_PATH;

abstract class AbstractZoomdataDriverTest extends AbstractZoomdataServerTest {

    private IZoomdataDriver driver;

    @BeforeMethod
    void setupDriver() {
        driver = new ZoomdataDriver("", "", false, getServer().getHostName(),
                getServer().getPort(), DEFAULT_PATH);
    }

    IZoomdataDriver getDriver() {
        return driver;
    }
}
