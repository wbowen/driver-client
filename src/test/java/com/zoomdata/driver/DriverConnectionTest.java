/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver;

import com.zoomdata.driver.model.ZoomdataVersion;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

public class DriverConnectionTest extends AbstractZoomdataDriverTest {

    @Test
    public void testGetZoomdataVersion() throws IOException {
        getServer().expect().withPath(BASE_API_PATH + "/version")
                .andReturn(200, readTestFile("api/versionTest.json")).always();
        ZoomdataVersion version = getDriver().getZoomdataVersion();
        assertEquals(version.getVersion(), "2.6.0");
    }

    @Test
    public void testValidConnection() throws IOException {
        getServer().expect().withPath(BASE_API_PATH)
                .andReturn(200, readTestFile("api/indexTest.json")).once();
        getServer().expect().get().withPath(BASE_WEB_SOCKET_PATH).andUpgradeToWebSocket().open();
        getDriver().connect();
        // No exception means we succeeded
    }

    @Test(expectedExceptions = IOException.class,
            expectedExceptionsMessageRegExp = "Received unexpected HTTP response code 401, expected 200")
    public void testInvalidAPIConnection() throws IOException {
        getServer().expect().withPath(BASE_API_PATH)
                .andReturn(401, null).always();
        getDriver().connect();
    }

    @Test(expectedExceptions = IOException.class)
    public void testInvalidWebSocketConnection() throws IOException {
        //getServer().enqueue(new MockResponse().setResponseCode(200));
        // TODO Test ws connection
        getDriver().connect();
        Assert.fail();
    }
}
