/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver;

import com.zoomdata.driver.model.Field;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

public class DriverFieldsTest extends AbstractZoomdataDriverTest {

    @Test
    public void testDescribeCollection() throws IOException {
        String sourceName = "SoCal Big Data Day";
        getServer().expect().withPath(BASE_API_PATH + "/sources?name=" + sourceName.replaceAll("\\s", "%20"))
                .andReturn(200, readTestFile("api/sourceByNameTest.json")).always();
        Set<Field> fields = getDriver().fields(sourceName);
        assertEquals(fields.size(), 26);
        fields.stream().forEach(f -> {
            assertNotNull(f.getName());
            assertNotNull(f.getLabel());
            assertNotNull(f.getRawField());
        });
    }

    @Test(expectedExceptions = IOException.class,
            expectedExceptionsMessageRegExp = "Could not find source with name Nonexistent")
    public void testDescribeCollectionInvalidName() throws IOException {
        String sourceName = "Nonexistent";
        getServer().expect().withPath(BASE_API_PATH + "/sources?name=" + sourceName)
                .andReturn(200, "[ ]").always();
        getDriver().fields(sourceName);
    }
}
