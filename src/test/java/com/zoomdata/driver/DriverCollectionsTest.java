/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver;

import com.zoomdata.driver.model.Collection;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class DriverCollectionsTest extends AbstractZoomdataDriverTest {

    @Test
    public void testListCollections() throws IOException {
        getServer().expect().withPath(BASE_API_PATH + "/sources")
                .andReturn(200, readTestFile("api/sourcesTest.json")).always();
        Set<Collection> collections = getDriver().listCollections();
        // One source of two is disabled. The driver should filter it
        assertEquals(collections.size(), 1);
        Collection collection = collections.iterator().next();
        assertEquals(collection.getId(), "58d60110b346f2cfaeadfc0e");
        assertEquals(collection.getName(), "SoCal Big Data Day");
        assertNotNull(collection.getRawSource());
    }

    @Test(expectedExceptions = IOException.class, expectedExceptionsMessageRegExp =
            "Received unexpected HTTP response code 500, expected 200")
    public void testListCollectionsError() throws IOException {
        getServer().expect().withPath(BASE_API_PATH + "/sources").andReturn(500, null).always();
        getDriver().listCollections();
    }

    @Test
    public void testListNoCollections() throws IOException {
        getServer().expect().withPath(BASE_API_PATH + "/sources").andReturn(200, "[]").always();
        assertTrue(getDriver().listCollections().isEmpty());
    }
}
