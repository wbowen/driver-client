/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata;

import com.zoomdata.client.IZoomdataAPIClient;
import com.zoomdata.client.ZoomdataAPIClient;
import com.zoomdata.websocket.IWebSocketClient;
import com.zoomdata.websocket.ZoomdataWebSocketClient;
import io.fabric8.mockwebserver.DefaultMockServer;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_API_BASE_PATH_SEGMENT;
import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_PATH;
import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_WEB_SOCKET_BASE_PATH_SEGMENT;

public abstract class AbstractZoomdataServerTest {

    private IZoomdataAPIClient zoomdataAPIClient;
    private IWebSocketClient webSocketClient;
    private DefaultMockServer server;
    public final static String BASE_API_PATH = "/" + DEFAULT_PATH + "/" + DEFAULT_API_BASE_PATH_SEGMENT;
    public final static String BASE_WEB_SOCKET_PATH = "/" + DEFAULT_PATH + "/" + DEFAULT_WEB_SOCKET_BASE_PATH_SEGMENT;

    @BeforeMethod
    public void setup() throws IOException {
        server = new DefaultMockServer();
        server.start();
        zoomdataAPIClient = new ZoomdataAPIClient(null, null, false, server.getHostName(),
                server.getPort(), DEFAULT_PATH);
        webSocketClient = new ZoomdataWebSocketClient("admin", "Z00mda1a", false,
                server.getHostName(), server.getPort(), DEFAULT_PATH);
    }

    @AfterMethod
    public void tearDown() throws IOException {
        server.shutdown();
    }

    public DefaultMockServer getServer() {
        return server;
    }

    public IZoomdataAPIClient getAPIClient() {
        return zoomdataAPIClient;
    }

    public IWebSocketClient getWebSocketClient() {
        return webSocketClient;
    }

    public static String readTestFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get("src/test/resources/" + path)));
    }
}
