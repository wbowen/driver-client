/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver;

import com.zoomdata.driver.model.Collection;
import com.zoomdata.driver.model.Field;
import com.zoomdata.driver.model.ZoomdataVersion;

import java.io.IOException;
import java.util.Set;

public interface IZoomdataDriver {

    void connect() throws IOException;

    ZoomdataVersion getZoomdataVersion() throws IOException;

    Set<Collection> listCollections() throws IOException;

    Set<Field> fields(String collectionName) throws IOException;
}
