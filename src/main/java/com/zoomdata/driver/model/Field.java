/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver.model;

import com.zoomdata.model.stream.ObjectField;
import lombok.Value;

@Value
public class Field {
    private final String name;
    private final String label;
    private final String type;
    private final ObjectField rawField;
}
