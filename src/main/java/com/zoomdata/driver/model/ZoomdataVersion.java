/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver.model;

import lombok.Value;

@Value
public class ZoomdataVersion {

    private final String version;
    private final String revision;
    private final String instanceId;
}
