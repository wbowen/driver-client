/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver.model;

import com.zoomdata.model.stream.StreamSource;
import lombok.Value;

@Value
public class Collection {

    private final String id;
    private final String name;
    private final StreamSource rawSource;
}
