/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.driver;

import com.zoomdata.client.IZoomdataAPIClient;
import com.zoomdata.client.ZoomdataAPIClient;
import com.zoomdata.driver.model.Collection;
import com.zoomdata.driver.model.Field;
import com.zoomdata.driver.model.ZoomdataVersion;
import com.zoomdata.model.stream.ObjectField;
import com.zoomdata.model.stream.StreamSource;
import com.zoomdata.websocket.IWebSocketClient;
import com.zoomdata.websocket.ZoomdataWebSocketClient;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_HOST;
import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_PATH;
import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_PORT;

public class ZoomdataDriver implements IZoomdataDriver {

    private final IZoomdataAPIClient apiClient;
    private final IWebSocketClient webSocketClient;

    public ZoomdataDriver(String userName, String password) {
        this(userName, password, false);
    }

    public ZoomdataDriver(String userName, String password, boolean isSecure) {
        this(userName, password, isSecure, DEFAULT_HOST, DEFAULT_PORT, DEFAULT_PATH);
    }

    public ZoomdataDriver(String userName, String password, boolean isSecure, String host, int port, String path) {
        apiClient = new ZoomdataAPIClient(userName, password, isSecure, host, port, path);
        webSocketClient = new ZoomdataWebSocketClient(userName, password, isSecure, host, port, path);
    }

    @Override
    public void connect() throws IOException {
        // Since the API is stateless, verify our credentials are valid by attempting a normal connection
        apiClient.index();
        try {
            webSocketClient.connect();
        } catch (Exception e) {
            throw new IOException("Error opening web socket connection", e);
        }
    }

    @Override
    public ZoomdataVersion getZoomdataVersion() throws IOException {
        return apiClient.zoomdataVersion();
    }

    @Override
    public Set<Collection> listCollections() throws IOException {
        return apiClient.listSources().stream()
                .filter(StreamSource::isEnabled) // Filter out disabled sources
                .map(s -> new Collection(s.getId(), s.getName(), s))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Field> fields(@NotNull String collectionName) throws IOException {
        StreamSource source;
        // Hack since our API returns an empty array instead of 404 or empty object
        try {
            source = apiClient.getSourceByName(collectionName);
        } catch (IOException e) {
            if (e.getMessage().contains("Can not deserialize instance of com.zoomdata.model.stream.StreamSource " +
                    "out of START_ARRAY token")) {
                throw new IOException("Could not find source with name " + collectionName);
            }
            throw e;
        }
        return source.getObjectFields().stream()
                .filter(ObjectField::isVisible)
                .map(f -> new Field(f.getName(), f.getLabel(), f.getType().toString(), f))
                .collect(Collectors.toSet());
    }
}
