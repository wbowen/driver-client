/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.websocket;

public interface IWebSocketClient {

    void connect() throws Exception;

    void fetch() throws Exception;

    void close() throws Exception;
}
