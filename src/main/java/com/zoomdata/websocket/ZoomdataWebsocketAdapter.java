/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.websocket;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;

import java.util.concurrent.CountDownLatch;

@Slf4j
public class ZoomdataWebsocketAdapter extends WebSocketAdapter {

    private Session session;
    private final CountDownLatch latch;

    public ZoomdataWebsocketAdapter() {
        this.latch = new CountDownLatch(1);
    }

    @Override
    public void onWebSocketConnect(Session session){
        this.session = session;
        log.info("Connected to websocket server");
        latch.countDown();
    }

    @Override
    public void onWebSocketText(String message){
        log.debug("Received web socket message: {}", message);
        /*JsonObject response = jsonParser.parse(message).getAsJsonObject();
        Queue<BaseRecordBatch> queue = requestsQueue.get(response.get("cid").getAsString());

        if(response.has("data")){
            JsonArray data = response.getAsJsonArray("data");
            List<BaseRecord> records = new LinkedList<>();
            data.forEach(d -> records.add(new ZoomdataRecord(d.getAsJsonArray())));
            BaseRecordBatch baseRecordBatch = new BaseRecordBatch(records, false);
            queue.offer(baseRecordBatch);
        }else if(response.has("status") && response.get("status").getAsString().equals("NOT_DIRTY_DATA")){
            queue.offer(BaseRecordBatch.LAST_BATCH);
        }*/
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason){
        log.info("Closing web socket with status {}, reason: {}", statusCode, reason);
    }

    @Override
    public void onWebSocketError(Throwable cause) {
        log.warn("Error in web socket", cause);
    }
}