/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.websocket;

import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Future;

import static com.zoomdata.client.HttpClientBuilder.encodeAuthorization;
import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_WEB_SOCKET_BASE_PATH_SEGMENT;

@Slf4j
public class ZoomdataWebSocketClient implements IWebSocketClient {

    private WebSocketClient webSocketClient;
    private Session session;
    private final ZoomdataWebsocketAdapter adapter;
    private final ClientUpgradeRequest upgradeRequest;
    private final URI uri;

    public ZoomdataWebSocketClient(String userName, String password, boolean isSecure, String host, int port, String path) {
        adapter = new ZoomdataWebsocketAdapter();
        try {
            uri = new URI(new HttpUrl.Builder().scheme(isSecure ? "https" : "http")
                    .host(host)
                    .port(port)
                    .addPathSegment(path)
                    .addPathSegment(DEFAULT_WEB_SOCKET_BASE_PATH_SEGMENT)
                    .build()
                    .toString()
                    // Hacky but HttpUrl won't accept "ws" so replace
                    .replaceFirst("^http", "ws")
            );
        } catch (URISyntaxException e) {
            throw new RuntimeException("Invalid web socket URI", e);
        }
        upgradeRequest = new ClientUpgradeRequest();
        upgradeRequest.setHeader("Authorization", "Basic " + encodeAuthorization(userName, password));
    }

    @Override
    public void connect() throws Exception {
        log.debug("Connecting to web socket with URI: {}", uri);
        webSocketClient = new WebSocketClient(new SslContextFactory(true));
        webSocketClient.start();
        Future<Session> future = webSocketClient.connect(adapter, uri, upgradeRequest);
        session = future.get();
        log.info("Established web socket session: {}", session);
    }

    // TODO Fetch Request
    @Override
    public void fetch() throws Exception {
        //StartVisCommand command = new StartVisCommand("", null, null, null);
    }

    @Override
    public void close() throws Exception {
        log.debug("Stopping web socket connection");
        if (webSocketClient == null) {
            log.warn("Received request to close web socket, but connection was never opened");
        } else {
            // If it's already stopped, nothing happens
            webSocketClient.stop();
        }
    }
}
