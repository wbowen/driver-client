/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.client.service;

import com.zoomdata.driver.model.ZoomdataVersion;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.Map;

public interface CoreService {

    @GET(".")   // This is how we say "same as base URL" for Retrofit, "/" or empty string does not work
    Call<Map<String, Object>> index();

    @GET("version")
    Call<ZoomdataVersion> version();
}
