/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.client.service;

import com.zoomdata.model.stream.StreamSource;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface SourceService {

    @GET("sources")
    Call<List<StreamSource>> listSources();

    @GET("sources")
    Call<StreamSource> getSourceByName(@Query("name") String name);
}
