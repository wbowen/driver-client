/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.client;

import com.zoomdata.driver.model.ZoomdataVersion;
import com.zoomdata.model.stream.StreamSource;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IZoomdataAPIClient {

    Map<String, Object> index() throws IOException;

    ZoomdataVersion zoomdataVersion() throws IOException;

    List<StreamSource> listSources() throws IOException;

    StreamSource getSourceByName(String name) throws IOException;
}
