/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.util.Base64;

public class HttpClientBuilder {

    private HttpClientBuilder() {}

    static OkHttpClient build(final String userName, final String password) {
        return new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest = chain.request().newBuilder()
                .header("Authorization", "Basic " + encodeAuthorization(userName, password))
                .build();
            return chain.proceed(newRequest);
        }).build();
    }

    public static String encodeAuthorization(String userName, String password) {
        return Base64.getEncoder().encodeToString((userName + ":" + password).getBytes());
    }
}