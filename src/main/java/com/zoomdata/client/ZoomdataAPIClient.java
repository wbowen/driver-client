/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.client;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zoomdata.client.service.CoreService;
import com.zoomdata.client.service.SourceService;
import com.zoomdata.driver.model.ZoomdataVersion;
import com.zoomdata.model.stream.StreamSource;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import static com.zoomdata.client.ZoomdataAPIDefaults.DEFAULT_API_BASE_PATH_SEGMENT;

@Slf4j
public class ZoomdataAPIClient implements IZoomdataAPIClient {

    private final Retrofit retrofit;
    private final SourceService sourceService;
    private final CoreService coreService;

    public ZoomdataAPIClient(String userName, String password, boolean isSecure, String host, int port, String path) {
        retrofit = new Retrofit.Builder()
            .baseUrl(
                    new HttpUrl.Builder()
                            .scheme(isSecure ? "https" : "http")
                            .host(host)
                            .port(port)
                            .addPathSegment(path)
                            .addPathSegments(DEFAULT_API_BASE_PATH_SEGMENT + "/")
                            .build()
            )
            .addConverterFactory(JacksonConverterFactory.create(getObjectMapper()))
            .client(HttpClientBuilder.build(userName, password))
            .build();
        sourceService = retrofit.create(SourceService.class);
        coreService = retrofit.create(CoreService.class);
    }

    private static ObjectMapper getObjectMapper() {
        return new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
    }

    private static void checkExpectedResponseCode(Response response) throws IOException {
        checkExpectedResponseCode(response, 200);
    }

    private static void checkExpectedResponseCode(Response response, int expectedCode) throws IOException {
        if (response.code() != expectedCode) {
            throw new IOException(String.format("Received unexpected HTTP response code %d, expected %d",
                    response.code(), expectedCode));
        }
    }

    public Map<String, Object> index() throws IOException {
        log.debug("Accessing index from URI: {}", coreService.index().request().url());
        Response<Map<String, Object>> response = coreService.index().execute();
        log.debug("Index response: {}", response);
        checkExpectedResponseCode(response);
        return response.body();
    }

    public ZoomdataVersion zoomdataVersion() throws IOException {
        log.debug("Accessing Zoomdata version from URI: {}", coreService.version().request().url());
        Response<ZoomdataVersion> response = coreService.version().execute();
        log.debug("Zoomdata version response: {}", response);
        checkExpectedResponseCode(response);
        return response.body();
    }

    @Override
    public List<StreamSource> listSources() throws IOException {
        log.debug("Listing sources from URI: {}", sourceService.listSources().request().url());
        Response<List<StreamSource>> response = sourceService.listSources().execute();
        log.debug("List sources response: {}", response);
        checkExpectedResponseCode(response);
        return response.body();
    }

    @Override
    public StreamSource getSourceByName(String name) throws IOException {
        log.debug("Requesting source with name {} from URI: {}", name,
                sourceService.getSourceByName(name).request().url());
        Response<StreamSource> response = sourceService.getSourceByName(name).execute();
        log.debug("Source by name response: {}", response);
        checkExpectedResponseCode(response);
        return response.body();
    }
}
