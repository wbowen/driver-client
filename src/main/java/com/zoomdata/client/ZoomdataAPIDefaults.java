/**
 * Copyright (C) Zoomdata, Inc. 2012-2017. All rights reserved.
 */
package com.zoomdata.client;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ZoomdataAPIDefaults {

    public static final String DEFAULT_HOST = "localhost";
    public static final int DEFAULT_PORT = 8080;
    public static final String DEFAULT_PATH = "zoomdata";
    public static final String DEFAULT_API_BASE_PATH_SEGMENT = "service";
    public static final String DEFAULT_WEB_SOCKET_BASE_PATH_SEGMENT = "websocket";
}
